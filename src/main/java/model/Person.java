package model;

public class Person {
    private int id;
    private String username;
    private String password;

    public Person(){}

    public Person(String username, String password, boolean administrator, int id){
        this.username = username;
        this.password = password;
        if(administrator)
            this.id = 5000 + id;
        else
            this.id = 3000 + id;
    }

    public String[] convert(){
        return new String[] {String.valueOf(id), username};
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
