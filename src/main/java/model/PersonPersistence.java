package model;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class PersonPersistence {
    private List<Person> people = new ArrayList();

    public PersonPersistence(){
        readXML();
    }

    public void addPerson(Person person){
        this.people.add(person);
        writeXML();
    }

    public void updatePerson(int id, String username, String password){
        for(Person p : people)
            if(p.getId() == id){
                if(!username.equals(""))
                    p.setUsername(username);
                if(!password.equals(""))
                    p.setPassword(password);
                writeXML();
                break;
            }

    }

    public void deletePerson(int id){
        Person person = new Person();
        for(Person p : people)
            if(p.getId() == id){
                person = p;
            }
        people.remove(person);

        writeXML();
    }

    public int getNumberOfPeople(){
        return people.get(people.size() - 1).getId() % 1000 + 1;
    }

    public List<Person> getPeople(){
        return people;
    }

    private void writeXML(){
        try{
            XmlMapper xmlMapper = new XmlMapper();
            xmlMapper.writeValue(new File("people.xml"), people);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private void readXML(){
        try{
            XmlMapper xmlMapper = new XmlMapper();
            File file = new File("people.xml");
            String xml = Files.readString(file.toPath());
            people = xmlMapper.readValue(xml,new TypeReference<List<Person>>() {});

        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
