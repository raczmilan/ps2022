package model;

public class Ticket {
    private int id;
    private int trainNumber;
    private double price;

    public Ticket(){}

    public Ticket(int id, int trainNumber, double price){
        this.id = id;
        this.trainNumber = trainNumber;
        this.price = price;
    }

    public String[] convert (){
        return new String[] {String.valueOf(id), String.valueOf(trainNumber), String.valueOf(price)};
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(int trainNumber) {
        this.trainNumber = trainNumber;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
