package model;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class TicketPersistence {
    private List<Ticket> tickets = new ArrayList<>();

    public TicketPersistence(){
        readXML();
    }

    public void addTicket(Ticket ticket){
        tickets.add(ticket);
        writeXML();
    }

    public int deleteTicket(int id){
        Ticket ticket = new Ticket();
        int number = 0;

        for(Ticket t : tickets){
            if(t.getId() == id) {
                ticket = t;
                number = t.getTrainNumber();
            }
        }

        tickets.remove(ticket);
        writeXML();
        return number;
    }

    public int deleteTicketsTrainNumber(int trainNumber){
        ArrayList<Ticket> ticketsToRemove = new ArrayList<>();

        for(Ticket t : tickets){
            if(t.getTrainNumber() == trainNumber)
                ticketsToRemove.add(t);
        }

        for(Ticket t : ticketsToRemove){
            tickets.remove(t);
        }

        writeXML();
        return ticketsToRemove.size();
    }

    public int getNumberOfTickets(){
        if(tickets.size() == 0)
            return 0;
        else
            return tickets.get(tickets.size() - 1).getId() + 1;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    private void writeXML(){
        try{
            XmlMapper xmlMapper = new XmlMapper();
            xmlMapper.writeValue(new File("tickets.xml"), tickets);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private void readXML(){
        try{
            XmlMapper xmlMapper = new XmlMapper();
            File file = new File("tickets.xml");
            String xml = Files.readString(file.toPath());
            tickets = xmlMapper.readValue(xml,new TypeReference<List<Ticket>>() {});

        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
