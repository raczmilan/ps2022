package model;

public class Train {
    private int number;
    private String pointOfDeparture;
    private String destination;
    private double duration;
    private double price;
    private int freeSeats;

    public Train(){}

    public Train(int number){
        this.number = number;
        this.pointOfDeparture = "";
        this.destination = "";
        this.duration = 0.0;
        this.price = 0.0;
        this.freeSeats = 0;
    }

    public Train(int number, String pointOfDeparture, String destination, double duration, double price, int freeSeats){
        this.number = number;
        this.pointOfDeparture = pointOfDeparture;
        this.destination = destination;
        this.duration = duration;
        this.price = price;
        this.freeSeats = freeSeats;
    }

    public String[] convert (){
        return new String[] {String.valueOf(number), pointOfDeparture, destination, String.valueOf(duration)};
    }

    public String[] convertWithPrice(){
        String[] st = convert();
        return new String[] {st[0], st[1], st[2], st[3], String.valueOf(price), String.valueOf(freeSeats)};
    }

    public boolean decrementSeatNr(){
        if(this.freeSeats > 0) {
            this.freeSeats -= 1;
            return true;
        }
        else
            return false;
    }

    public void incrementSeatNr(int seats){
        this.freeSeats += seats;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getPointOfDeparture() {
        return pointOfDeparture;
    }

    public void setPointOfDeparture(String pointOfDeparture) {
        this.pointOfDeparture = pointOfDeparture;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getFreeSeats() {
        return freeSeats;
    }

    public void setFreeSeats(int freeSeats) {
        this.freeSeats = freeSeats;
    }
}
