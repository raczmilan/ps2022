package model;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class TrainPersistence {
    private List<Train> trains = new ArrayList();

    public TrainPersistence(){
        readXML();
    }

    public void addTrain(Train train){
        this.trains.add(train);
        writeXML();
    }

    public void updateTrain(int number, String pointOfDeparture, String destination, double duration, double price, int freeSeats){
        for(Train t : trains)
            if(t.getNumber() == number){
                if(!pointOfDeparture.equals(""))
                    t.setPointOfDeparture(pointOfDeparture);
                if(!destination.equals(""))
                    t.setDestination(destination);
                if(duration >= 0.0)
                    t.setDuration(duration);
                if(price >= 0.0)
                    t.setPrice(price);
                if(freeSeats >= 0)
                    t.setFreeSeats(freeSeats);
                writeXML();
                break;
            }
    }

    public void deleteTrain(int number){
        Train train = new Train();
        for(Train t : trains)
            if(t.getNumber() == number){
                train = t;
            }

        trains.remove(train);
        writeXML();
    }

    public boolean decrementSeatNumber(int number){
        for(Train t : trains)
            if(t.getNumber() == number){
                boolean s = t.decrementSeatNr();
                writeXML();
                return s;
            }
        return false;
    }

    public void incrementSeatNumber(int number, int seats){
        for(Train t : trains)
            if(t.getNumber() == number){
                t.incrementSeatNr(seats);
            }
        writeXML();
    }

    public Train getTrain(int number){
        for(Train t : trains)
            if(t.getNumber() == number){
                return t;
            }

        return null;
    }

    public List<Train> getTrains(){
        return trains;
    }

    private void writeXML(){
        try{
            XmlMapper xmlMapper = new XmlMapper();
            xmlMapper.writeValue(new File("trains.xml"), trains);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private void readXML(){
        try{
            XmlMapper xmlMapper = new XmlMapper();
            File file = new File("trains.xml");
            String xml = Files.readString(file.toPath());
            trains = xmlMapper.readValue(xml,new TypeReference<List<Train>>() {});

        } catch (IOException e){
            e.printStackTrace();
        }
    }


}
