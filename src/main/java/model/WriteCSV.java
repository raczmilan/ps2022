package model;

import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class WriteCSV extends WriteFile{

    private static FileWriter file;

    public void writeFile(ArrayList<String[]> trains){

        try {
            file = new FileWriter("raport.csv");
            CSVWriter writer = new CSVWriter(file, ',');

            writer.writeNext(new String[] {"Numar tren", "Statie de plecare", "Destinatie", "Durata (ore)", "Pret", "Locuri disponibile"}, false);
            writer.writeAll(trains, false);

            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
