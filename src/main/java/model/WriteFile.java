package model;

import java.util.ArrayList;

public abstract class WriteFile {

    public WriteFile(){}

    public abstract void writeFile(ArrayList<String[]> trains);
}
