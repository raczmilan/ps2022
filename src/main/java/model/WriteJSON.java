package model;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class WriteJSON extends WriteFile{
    private static FileWriter file;

    public void writeFile(ArrayList<String[]> trains){
        JSONObject obj = new JSONObject();

        JSONArray train;
        for(String[] i : trains) {
            train = new JSONArray();
            train.add("Stație de plecare: " + i[1]);
            train.add("Destinație: " + i[2]);
            train.add("Durata în ore: " + i[3]);
            train.add("Preț: " + i[4]);
            train.add("Număr de locuri libere: " + i[5]);

            obj.put("Tren cu numărul " + i[0], train);
        }
        try {
            file = new FileWriter("raport.json");
            file.write(obj.toJSONString());

        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {
                file.flush();
                file.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
