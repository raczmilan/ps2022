package presenter;

import model.Person;
import model.PersonPersistence;
import view.AdministratorView;
import view.IAdministratorView;

import java.util.ArrayList;
import java.util.List;

public class AdministratorPresenter {

    private IAdministratorView administratorView;
    private PersonPersistence pp;

    public AdministratorPresenter(AdministratorView av){
        this.administratorView = av;
        this.pp = new PersonPersistence();
    }

    public void addPerson(String username, String password, boolean administrator){
        List<Person> people = pp.getPeople();
        boolean unique = true;

        for(Person p : people){
            if(p.getUsername().equals(username))
                unique = false;
        }

        if(!username.equals("") && !password.equals("") && unique)
            pp.addPerson(new Person(username, password, administrator, pp.getNumberOfPeople()));

        showPeople();
    }

    public void updatePerson(String id, String username, String password){
        try {
            int nr = Integer.parseInt(id);
            boolean unique = true;

            if (!username.equals("")) {
                List<Person> people = pp.getPeople();
                for (Person p : people) {
                    if (p.getUsername().equals(username))
                        unique = false;
                }
            }

            if (unique) {
                pp.updatePerson(nr, username, password);
            }

            showPeople();
        } catch (NumberFormatException e) {
            administratorView.setId("Introduceți un număr");
        }
    }

    public void deletePerson(String id){
        try {
            pp.deletePerson(Integer.parseInt(id));

            showPeople();
        } catch (NumberFormatException e) {
            administratorView.setId("Introduceți un număr");
        }
    }

    public void showPeople(){
        String[] columns = new String[] {"Id", "Username"};

        List<Person> people = pp.getPeople();
        ArrayList<String[]> tr = new ArrayList<>();

        for(Person person : people) {
            tr.add(person.convert());
        }

        administratorView.setTable2(tr, columns);

    }

}
