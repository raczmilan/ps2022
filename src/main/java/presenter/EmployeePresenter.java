package presenter;

import model.*;
import view.EmployeeView;
import view.IEmployeeView;

import java.util.ArrayList;
import java.util.List;

public class EmployeePresenter {

    private IEmployeeView employeeView;
    private TrainPersistence tp;
    private TicketPersistence tip;
    private WriteFile jf;
    private WriteFile cf;

    public EmployeePresenter(EmployeeView ev){
        this.employeeView = ev;
        this.tp = new TrainPersistence();
        this.tip = new TicketPersistence();
        this.jf = new WriteJSON();
        this.cf = new WriteCSV();
    }

    public void addTrain(String number, String pointOfDeparture, String destination, String duration, String price, String freeSeats){
        List<Train> trains = tp.getTrains();
        boolean unique = true;

        try {
            int nr = Integer.parseInt(number);
            for (Train t : trains) {
                if (t.getNumber() == nr)
                    unique = false;
            }

            /*if(unique && pointOfDeparture.equals("") && destination.equals("") && duration.equals("") && price.equals("") && freeSeats.equals("")) {
                tp.addTrain(new Train());
                return;
            }*/

            double dur, pr;
            int freeS;

            try {
                dur = Double.parseDouble(duration);
            } catch (NumberFormatException e) {
                dur = 0.0;
            }

            try {
                pr = Double.parseDouble(price);
            } catch (NumberFormatException e) {
                pr = 0.0;
            }

            try {
                freeS = Integer.parseInt(freeSeats);
            } catch (NumberFormatException e) {
                freeS = 0;
            }

            if(unique)
                tp.addTrain(new Train(nr, pointOfDeparture, destination, dur, pr, freeS));

        } catch (NumberFormatException e) {
            employeeView.setNumber1("Număr invalid");
        }
    }

    public void updateTrain(String number, String pointOfDeparture, String destination, String duration, String price, String freeSeats){
        try {
            int nr = Integer.parseInt(number);
            double dur, pr;
            int freeS;

            try {
                dur = Double.parseDouble(duration);
            } catch (NumberFormatException e) {
                dur = -1.0;
            }

            try {
                pr = Double.parseDouble(price);
            } catch (NumberFormatException e) {
                pr = -1.0;
            }

            try {
                freeS = Integer.parseInt(freeSeats);
            } catch (NumberFormatException e) {
                freeS = -1;
            }

            tp.updateTrain(nr, pointOfDeparture, destination, dur, pr, freeS);

        } catch (NumberFormatException e) {
            employeeView.setNumber1("Număr invalid");
        }
    }

    public void deleteTrain(String number){
        try {
            tp.deleteTrain(Integer.parseInt(number));
        } catch (NumberFormatException e) {
            employeeView.setNumber1("Număr invalid");
        }
    }

    public void addTicket(String trainNumber){
        int id = tip.getNumberOfTickets(), trN = 0;

        try{
            trN = Integer.parseInt(trainNumber);
        } catch (NumberFormatException e) {
            employeeView.setNumber2("Număr invalid");
            return;
        }

        if(tp.decrementSeatNumber(trN)) {
            Train train = tp.getTrain(trN);
            if(train != null)
                tip.addTicket(new Ticket(id, trN, train.getPrice()));
        }
        else
            employeeView.setNumber2("Operația a eșuat");

        showTickets();
    }

    public void deleteTicket(String id){
        try {
            int trainNumber = tip.deleteTicket(Integer.parseInt(id));
            tp.incrementSeatNumber(trainNumber, 1);
        } catch (NumberFormatException e) {
            employeeView.setId("Număr invalid");
        }
        showTickets();
    }

    public void deleteTicketsTrainNumber(String trainNumber){
        try {
            int number = tip.deleteTicketsTrainNumber(Integer.parseInt(trainNumber));
            tp.incrementSeatNumber(Integer.parseInt(trainNumber), number);
        } catch (NumberFormatException e) {
            employeeView.setId("Număr invalid");
        }
        showTickets();
    }

    public void showTickets(){
        String[] columns = new String[] {"Id", "Număr tren", "Preț"};

        List<Ticket> tickets = tip.getTickets();
        ArrayList<String[]> ti = new ArrayList<>();

        for(Ticket ticket : tickets) {
            ti.add(ticket.convert());
        }

        employeeView.setTable1(ti, columns);

    }

    public void writeCSV(){
        ArrayList<String[]> tl = new ArrayList<>();
        List<Train> trains = tp.getTrains();

        for(Train train : trains){
            tl.add(train.convertWithPrice());
        }

        cf.writeFile(tl);
    }

    public void writeJSON(){
        ArrayList<String[]> tl = new ArrayList<>();
        List<Train> trains = tp.getTrains();

        for(Train train : trains){
            tl.add(train.convertWithPrice());
        }

        jf.writeFile(tl);
    }

}
