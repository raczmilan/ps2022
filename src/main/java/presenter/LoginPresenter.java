package presenter;

import model.Person;
import model.PersonPersistence;
import view.*;

import java.util.List;

public class LoginPresenter {

    private ILoginView loginView;
    private PersonPersistence pp;

    public LoginPresenter(LoginView lv){
        this.loginView = lv;
        this.pp = new PersonPersistence();
    }

    public boolean login(String username, String password){
        int id = verifyPerson(username, password);
        TravelerView aw;
        if(id >= 5000) {
            aw = new AdministratorView();
            return true;
        }
        else if(id >= 3000) {
            aw = new EmployeeView();
            return true;
        }
        else
            return false;
    }

    private int verifyPerson(String username, String password){
       List<Person> personList = pp.getPeople();

       for(Person p : personList){
           if(p.getUsername().equals(username) && p.getPassword().equals(password))
               return p.getId();
       }

       return 0;
    }
}
