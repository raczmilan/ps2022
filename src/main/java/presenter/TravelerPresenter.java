package presenter;

import model.Train;
import model.TrainPersistence;
import view.ITravelerView;
import view.LoginView;
import view.TravelerView;

import java.util.ArrayList;
import java.util.List;

public class TravelerPresenter {

    private ITravelerView travelerView;
    private TrainPersistence tp;

    public TravelerPresenter(TravelerView tv){
        this.travelerView = tv;
        this.tp = new TrainPersistence();
    }

    public void searchTrains(String trainNumber, String pointOfDeparture, String destination, String duration, boolean withPrice){
        String[] columns = new String[] {"Număr tren", "Stație de plecare", "Destinație", "Durata (ore)"};
        String[] columns2 = new String[] {"Număr tren", "Stație de plecare", "Destinație", "Durata (ore)", "Preț", "Locuri disponibile"};

        List<Train> trains = tp.getTrains();
        ArrayList<String[]> tr = new ArrayList<>();

        for(Train train : trains) {
            if (trainNumber.equals("")) {
                boolean dep = false, des = false, dur = false;

                if (pointOfDeparture.equals("") || pointOfDeparture.equals(train.getPointOfDeparture())) {
                    dep = true;
                }

                if(destination.equals("") || destination.equals(train.getDestination())){
                    des = true;
                }

                if (duration.equals("") || Double.parseDouble(duration) == train.getDuration()) {
                    dur = true;
                }

                if(dep && des && dur) {
                    if(withPrice)
                        tr.add(train.convertWithPrice());
                    else
                        tr.add(train.convert());
                }

            } else {
                try {
                    if (Integer.parseInt(trainNumber) == train.getNumber()) {
                        travelerView.setTable(train.convert(), columns);
                        return;
                    }
                } catch (NumberFormatException e) {
                    travelerView.setNumber("Număr invalid");
                }
            }
        }

        if(withPrice)
            travelerView.setTable(tr, columns2);
        else
            travelerView.setTable(tr, columns);
    }

    public void goToLogin(){
        LoginView lw = new LoginView();
    }
}
