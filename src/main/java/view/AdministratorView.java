package view;

import presenter.AdministratorPresenter;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class AdministratorView extends TravelerView implements IAdministratorView{
    private final JLabel lMain = new JLabel ("Operații CRUD");

    private final JLabel l1 = new JLabel ("ID: ");
    private final JLabel l2 = new JLabel ("Nume de utilizator: ");
    private final JLabel l3 = new JLabel ("Parolă: ");

    private JTextArea tf1 = new JTextArea(1,10);
    private JTextArea tf2 = new JTextArea(1,10);
    private JTextArea tf3 = new JTextArea(1,10);

    private JButton b1 = new JButton("Adăugare utilizator nou");
    private JButton b2 = new JButton("Stergere utilizator după id");
    private JButton b3 = new JButton("Actualizare informatii utilizator după id");
    private JButton b4 = new JButton("Vizualizare utilizatori");

    private JCheckBox checkBox = new JCheckBox("Administrator");

    private DefaultTableModel model = new DefaultTableModel();
    private JTable t = new JTable(model);

    public AdministratorView() {
        super();

        JFrame frame = new JFrame ("Administratori");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(550, 700);

        addB1Listener();
        addB2Listener();
        addB3Listener();
        addB4Listener();

        JPanel panel = new JPanel();
        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();
        JPanel panel4 = new JPanel();
        JPanel panel5 = new JPanel();

        t.setBounds(20, 30, 100, 100);
        JScrollPane sp = new JScrollPane(t);

        FlowLayout flLayout = new FlowLayout();
        GridLayout grLayout = new GridLayout(2,2,4,4);

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
        panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));
        panel3.setLayout(flLayout);
        panel4.setLayout(flLayout);
        panel5.setLayout(grLayout);

        panel1.add(l1);
        panel1.add(Box.createRigidArea(new Dimension(0,2)) );
        panel1.add(l2);
        panel1.add(Box.createRigidArea(new Dimension(0,2)) );
        panel1.add(l3);

        panel2.add(tf1);
        panel2.add(Box.createRigidArea(new Dimension(0,2)) );
        panel2.add(tf2);
        panel2.add(Box.createRigidArea(new Dimension(0,2)) );
        panel2.add(tf3);

        panel3.add(panel1);
        panel3.add(panel2);

        panel4.add(checkBox);

        panel5.add(b1);
        panel5.add(b2);
        panel5.add(b3);
        panel5.add(b4);

        panel.add( Box.createRigidArea(new Dimension(0,5)) );
        lMain.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(lMain);
        panel.add( Box.createRigidArea(new Dimension(0,5)) );
        panel.add(panel3);
        panel.add( Box.createRigidArea(new Dimension(0,10)) );
        panel.add(panel4);
        panel.add( Box.createRigidArea(new Dimension(0,10)) );
        panel.add(panel5);
        panel.add( Box.createRigidArea(new Dimension(0,15)) );
        panel.add(sp);

        JScrollPane pane = new JScrollPane(panel);
        pane.getVerticalScrollBar().setUnitIncrement(16);
        frame.setContentPane(pane);
        frame.setVisible(true);
    }

    public void setId(String text){
        tf1.setText(text);
    }

    public String getId() {
        return tf1.getText();
    }
    public String getUsername() {
        return tf2.getText();
    }
    public String getPassword() { return tf3.getText(); }

    public void setTable2(ArrayList<String[]> personInfo, String[] columns){
        model.setColumnCount(0);
        model.setRowCount(0);

        for(String j : columns){
            model.addColumn(j);
        }

        for(String[] i : personInfo) {
            model.addRow(i);
        }
    }

    private AdministratorView getAdministratorView(){
        return this;
    }

    private void addB1Listener() {
        b1.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e){
                AdministratorPresenter ap = new AdministratorPresenter(getAdministratorView());
                ap.addPerson(getUsername(), getPassword(), checkBox.isSelected());
            }
        });
    }

    private void addB2Listener() {
        b2.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e){
                AdministratorPresenter ap = new AdministratorPresenter(getAdministratorView());
                ap.deletePerson(getId());
            }
        });
    }

    private void addB3Listener() {
        b3.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e){
                AdministratorPresenter ap = new AdministratorPresenter(getAdministratorView());
                ap.updatePerson(getId(), getUsername(), getPassword());
            }
        });
    }

    private void addB4Listener() {
        b4.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e){
                AdministratorPresenter ap = new AdministratorPresenter(getAdministratorView());
                ap.showPeople();
            }
        });
    }

}
