package view;

import presenter.EmployeePresenter;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class EmployeeView extends TravelerView implements IEmployeeView{
    private final JLabel lMain1 = new JLabel ("Operații CRUD / Vânzare bilete");
    private final JLabel lMain2 = new JLabel ("Salvare rapoarte");

    private final JLabel l1 = new JLabel ("Număr tren: ");
    private final JLabel l2 = new JLabel ("Stație de plecare: ");
    private final JLabel l3 = new JLabel ("Destinație: ");
    private final JLabel l4 = new JLabel ("Durată: ");
    private final JLabel l5 = new JLabel ("Preț bilet: ");
    private final JLabel l6 = new JLabel ("Bilete disponibile: ");

    private final JLabel l7 = new JLabel ("Număr bilet: ");
    private final JLabel l8 = new JLabel ("Număr tren: ");

    private JTextArea tf1 = new JTextArea(1,10);
    private JTextArea tf2 = new JTextArea(1,10);
    private JTextArea tf3 = new JTextArea(1,10);
    private JTextArea tf4 = new JTextArea(1,10);
    private JTextArea tf5 = new JTextArea(1,10);
    private JTextArea tf6 = new JTextArea(1,10);
    private JTextArea tf7 = new JTextArea(1,10);
    private JTextArea tf8 = new JTextArea(1,10);

    private JButton b1 = new JButton("Adăugare tren nou");
    private JButton b2 = new JButton("Stergere tren după număr");
    private JButton b3 = new JButton("Actualizare informatii tren după număr");
    private JButton b4 = new JButton("Vizualizare bilete vândute");
    private JButton b5 = new JButton("Ștergere bilet după număr");
    private JButton b6 = new JButton("Ștergere bilet după numărul trenului");
    private JButton b7 = new JButton("Vânzare bilet către un călător");
    private JButton b8 = new JButton("Salvare raport cu format .csv");
    private JButton b9 = new JButton("Salvare raport cu format .json");

    private DefaultTableModel model = new DefaultTableModel();
    private JTable t = new JTable(model);

    public EmployeeView(){
        super();

        JFrame frame = new JFrame ("Angajați");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(775, 775);
        addB1Listener();
        addB2Listener();
        addB3Listener();
        addB4Listener();
        addB5Listener();
        addB6Listener();
        addB7Listener();
        addB8Listener();
        addB9Listener();

        JPanel panel = new JPanel();
        JPanel mainPanel1 = new JPanel();
        JPanel mainPanel2 = new JPanel();
        JPanel mainPanel3 = new JPanel();
        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();
        JPanel panel4 = new JPanel();

        t.setBounds(20, 30, 100, 100);
        JScrollPane sp = new JScrollPane(t);

        FlowLayout flLayout = new FlowLayout();
        GridLayout grLayout = new GridLayout(3,2,4,4);

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        mainPanel1.setLayout(new BoxLayout(mainPanel1, BoxLayout.Y_AXIS));
        mainPanel2.setLayout(new BoxLayout(mainPanel2, BoxLayout.Y_AXIS));
        mainPanel3.setLayout(new BoxLayout(mainPanel3, BoxLayout.X_AXIS));
        panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
        panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));
        panel3.setLayout(flLayout);
        panel4.setLayout(grLayout);

        panel1.add(l1);
        panel1.add(Box.createRigidArea(new Dimension(0,2)) );
        panel1.add(l2);
        panel1.add(Box.createRigidArea(new Dimension(0,2)) );
        panel1.add(l3);
        panel1.add(Box.createRigidArea(new Dimension(0,2)) );
        panel1.add(l4);
        panel1.add(Box.createRigidArea(new Dimension(0,2)) );
        panel1.add(l5);
        panel1.add(Box.createRigidArea(new Dimension(0,2)) );
        panel1.add(l6);
        panel1.add(Box.createRigidArea(new Dimension(0,15)) );
        panel1.add(l7);
        panel1.add(Box.createRigidArea(new Dimension(0,2)) );
        panel1.add(l8);

        panel2.add(tf1);
        panel2.add(Box.createRigidArea(new Dimension(0,2)) );
        panel2.add(tf2);
        panel2.add(Box.createRigidArea(new Dimension(0,2)) );
        panel2.add(tf3);
        panel2.add(Box.createRigidArea(new Dimension(0,2)) );
        panel2.add(tf4);
        panel2.add(Box.createRigidArea(new Dimension(0,2)) );
        panel2.add(tf5);
        panel2.add(Box.createRigidArea(new Dimension(0,2)) );
        panel2.add(tf6);
        panel2.add(Box.createRigidArea(new Dimension(0,15)) );
        panel2.add(tf7);
        panel2.add(Box.createRigidArea(new Dimension(0,2)) );
        panel2.add(tf8);

        panel3.add(panel1);
        panel3.add(panel2);

        panel4.add(b1);
        panel4.add(b2);
        panel4.add(b3);
        panel4.add(b4);
        panel4.add(b5);
        panel4.add(b6);

        mainPanel1.add( Box.createRigidArea(new Dimension(0,5)) );
        lMain1.setAlignmentX(Component.CENTER_ALIGNMENT);
        mainPanel1.add(lMain1);
        mainPanel1.add( Box.createRigidArea(new Dimension(0,5)) );
        mainPanel1.add(panel3);
        mainPanel1.add( Box.createRigidArea(new Dimension(0,5)) );
        mainPanel1.add(panel4);
        mainPanel1.add( Box.createRigidArea(new Dimension(0,5)) );
        b7.setAlignmentX(Component.CENTER_ALIGNMENT);
        mainPanel1.add(b7);

        mainPanel2.add( Box.createRigidArea(new Dimension(0,30)) );
        lMain2.setAlignmentX(Component.CENTER_ALIGNMENT);
        mainPanel2.add(lMain2);
        mainPanel2.add( Box.createRigidArea(new Dimension(0,5)) );
        b8.setAlignmentX(Component.CENTER_ALIGNMENT);
        mainPanel2.add(b8);
        mainPanel2.add( Box.createRigidArea(new Dimension(0,5)) );
        b9.setAlignmentX(Component.CENTER_ALIGNMENT);
        mainPanel2.add(b9);

        mainPanel3.add( Box.createRigidArea(new Dimension(5,0)) );
        mainPanel3.add(mainPanel1);
        mainPanel3.add( Box.createRigidArea(new Dimension(25,0)) );
        mainPanel3.add(mainPanel2);
        mainPanel3.add( Box.createRigidArea(new Dimension(5,0)) );

        panel.add(mainPanel3);
        panel.add( Box.createRigidArea(new Dimension(0,5)) );
        panel.add(sp);

        JScrollPane pane = new JScrollPane(panel);
        pane.getVerticalScrollBar().setUnitIncrement(16);
        frame.setContentPane(pane);
        frame.setVisible(true);
    }

    public void setNumber1(String text){tf1.setText(text);}
    public void setId(String text){tf7.setText(text);}
    public void setNumber2(String text){tf8.setText(text);}

    public String getNumber1() {
        return tf1.getText();
    }
    public String getPointOfDeparture1() {
        return tf2.getText();
    }
    public String getDestination1() { return tf3.getText(); }
    public String getDuration1() {
        return tf4.getText();
    }
    public String getPrice1() {
        return tf5.getText();
    }
    public String getFreeSeats1() {
        return tf6.getText();
    }

    public String getId() {
        return tf7.getText();
    }
    public String getNumber2() {
        return tf8.getText();
    }

    public void setTable1(ArrayList<String[]> ticketInfo, String[] columns){
        model.setColumnCount(0);
        model.setRowCount(0);

        for(String j : columns){
            model.addColumn(j);
        }

        for(String[] i : ticketInfo) {
            model.addRow(i);
        }
    }

    private EmployeeView getEmployeeView(){
        return this;
    }

    private void addB1Listener() {
        b1.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e){
                EmployeePresenter ep = new EmployeePresenter(getEmployeeView());
                ep.addTrain(getNumber1(), getPointOfDeparture1(), getDestination1(), getDuration1(), getPrice1(), getFreeSeats1());
            }
        });
    }

    private void addB2Listener() {
        b2.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e){
                EmployeePresenter ep = new EmployeePresenter(getEmployeeView());
                ep.deleteTrain(getNumber1());
            }
        });
    }

    private void addB3Listener() {
        b3.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e){
                EmployeePresenter ep = new EmployeePresenter(getEmployeeView());
                ep.updateTrain(getNumber1(), getPointOfDeparture1(), getDestination1(), getDuration1(), getPrice1(), getFreeSeats1());
            }
        });
    }

    private void addB4Listener() {
        b4.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e){
                EmployeePresenter ep = new EmployeePresenter(getEmployeeView());
                ep.showTickets();
            }
        });
    }

    private void addB5Listener() {
        b5.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e){
                EmployeePresenter ep = new EmployeePresenter(getEmployeeView());
                ep.deleteTicket(getId());
            }
        });
    }

    private void addB6Listener() {
        b6.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e){
                EmployeePresenter ep = new EmployeePresenter(getEmployeeView());
                ep.deleteTicketsTrainNumber(getNumber2());
            }
        });
    }

    private void addB7Listener() {
        b7.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e){
                EmployeePresenter ep = new EmployeePresenter(getEmployeeView());
                ep.addTicket(getNumber2());
            }
        });
    }

    private void addB8Listener() {
        b8.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e){
                EmployeePresenter ep = new EmployeePresenter(getEmployeeView());
                ep.writeCSV();
            }
        });
    }

    private void addB9Listener() {
        b9.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e){
                EmployeePresenter ep = new EmployeePresenter(getEmployeeView());
                ep.writeJSON();
            }
        });
    }

}
