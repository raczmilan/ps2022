package view;

import java.util.ArrayList;

public interface IAdministratorView {

    void setId(String text);
    String getId();
    String getUsername();
    String getPassword();
    void setTable2(ArrayList<String[]> personInfo, String[] columns);

}
