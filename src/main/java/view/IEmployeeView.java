package view;

import java.util.ArrayList;

public interface IEmployeeView {

    void setNumber1(String text);
    void setNumber2(String text);
    void setId(String text);
    String getNumber1();
    String getPointOfDeparture1();
    String getDestination1();
    String getDuration1();
    String getPrice1();
    String getFreeSeats1();
    String getId();
    String getNumber2();
    void setTable1(ArrayList<String[]> ticketInfo, String[] columns);


}
