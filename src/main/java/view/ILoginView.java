package view;

public interface ILoginView {
    String getUsername();
    String getPassword();
}
