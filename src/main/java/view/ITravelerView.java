package view;

import java.util.ArrayList;

public interface ITravelerView {
    void setNumber(String text);
    String getPointOfDeparture();
    String getDestination();
    String getDuration();
    String getNumber();
    void setTable(ArrayList<String[]> trainInfo, String[] columns);
    void setTable(String[] trainInfo, String[] columns);
}
