package view;

import presenter.LoginPresenter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginView implements ILoginView{
    private JFrame frame = new JFrame ("Login");
    private final JLabel l1 = new JLabel ("Autentificare");
    private final JLabel l2 = new JLabel ("Nume de utilizator: ");
    private final JLabel l3 = new JLabel ("Parolă: ");

    private JTextArea tf1 = new JTextArea(1,10);
    private JTextArea tf2 = new JTextArea(1,10);

    private JButton b1 = new JButton("Autentificare");

    public LoginView() {
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(300, 300);
        addB1Listener();

        JPanel panel = new JPanel();
        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();
        JPanel panel4 = new JPanel();

        FlowLayout flLayout = new FlowLayout();

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
        panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));
        panel3.setLayout(flLayout);
        panel4.setLayout(flLayout);

        panel1.add(l2);
        panel1.add(Box.createRigidArea(new Dimension(0,3)) );
        panel1.add(l3);

        panel2.add(tf1);
        panel2.add(Box.createRigidArea(new Dimension(0,3)) );
        panel2.add(tf2);

        panel3.add(panel1);
        panel3.add(panel2);

        panel4.add(b1);

        panel.setAlignmentX(JPanel.CENTER_ALIGNMENT);

        panel.add( Box.createRigidArea(new Dimension(0,5)) );
        l1.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(l1);
        panel.add( Box.createRigidArea(new Dimension(0,15)) );
        panel.add(panel3);
        panel.add( Box.createRigidArea(new Dimension(0,5)) );
        panel.add(panel4);

        JScrollPane pane = new JScrollPane(panel);
        pane.getVerticalScrollBar().setUnitIncrement(16);
        frame.setContentPane(pane);
        frame.setVisible(true);
    }

    public String getUsername() {
        return tf1.getText();
    }
    public String getPassword() {
        return tf2.getText();
    }

    private LoginView getLoginView(){
        return this;
    }

    private void addB1Listener() {
        b1.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e){
                LoginPresenter lp = new LoginPresenter(getLoginView());
                boolean success = lp.login(getUsername(), getPassword());
                if(success)
                    frame.dispose();
            }
        });
    }
}
