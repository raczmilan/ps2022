package view;

import presenter.TravelerPresenter;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class TravelerView implements ITravelerView{

    private JFrame frame = new JFrame ("Călători");
    private final JLabel l1 = new JLabel ("Căutare tren");
    private final JLabel l2 = new JLabel ("Stație de plecare: ");
    private final JLabel l3 = new JLabel ("Destinație: ");
    private final JLabel l4 = new JLabel ("Durată: ");
    private final JLabel l5 = new JLabel ("Număr tren: ");
    private JCheckBox checkBox = new JCheckBox("Afișare preț și disponibilitate");
    private JTextArea tf1 = new JTextArea(1,10);
    private JTextArea tf2 = new JTextArea(1,10);
    private JTextArea tf3 = new JTextArea(1,10);
    private JTextArea tf4 = new JTextArea(1,10);
    private JButton b1 = new JButton("Vizualizare");
    private JButton b2 = new JButton("Login");
    private DefaultTableModel model = new DefaultTableModel();
    private JTable t = new JTable(model);

    public TravelerView() {
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(280, 400);
        addB1Listener();
        addB2Listener();

        JPanel panel = new JPanel();
        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();

        t.setBounds(30, 40, 250, 300);
        JScrollPane sp = new JScrollPane(t);

        FlowLayout flLayout = new FlowLayout();

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
        panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));
        panel3.setLayout(flLayout);

        l1.setAlignmentX(Component.CENTER_ALIGNMENT);

        panel1.add(l2);
        panel1.add(Box.createRigidArea(new Dimension(0,2)) );
        panel1.add(l3);
        panel1.add(Box.createRigidArea(new Dimension(0,2)) );
        panel1.add(l4);
        panel1.add(Box.createRigidArea(new Dimension(0,2)) );
        panel1.add(l5);

        panel2.add(tf1);
        panel2.add(Box.createRigidArea(new Dimension(0,2)) );
        panel2.add(tf2);
        panel2.add(Box.createRigidArea(new Dimension(0,2)) );
        panel2.add(tf3);
        panel2.add(Box.createRigidArea(new Dimension(0,2)) );
        panel2.add(tf4);

        panel3.add(panel1);
        panel3.add(panel2);

        panel.add( Box.createRigidArea(new Dimension(0,5)) );
        l1.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(l1);
        panel.add( Box.createRigidArea(new Dimension(0,5)) );
        panel.add(panel3);
        checkBox.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(checkBox);
        panel.add( Box.createRigidArea(new Dimension(0,5)) );
        b1.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(b1);
        panel.add( Box.createRigidArea(new Dimension(0,5)) );
        b2.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(b2);
        panel.add( Box.createRigidArea(new Dimension(0,10)) );
        panel.add(sp);

        frame.setContentPane(panel);
        frame.setVisible(true);
    }

    public void setNumber(String text) {tf4.setText(text);}

    public String getPointOfDeparture() {
        return tf1.getText();
    }
    public String getDestination() {
        return tf2.getText();
    }
    public String getDuration() { return tf3.getText(); }
    public String getNumber() {
        return tf4.getText();
    }

    public void setTable(ArrayList<String[]> trainInfo, String[] columns){
        model.setColumnCount(0);
        model.setRowCount(0);

        for(String j : columns){
            model.addColumn(j);
        }

        for(String[] i : trainInfo) {
            model.addRow(i);
        }
    }

    public void setTable(String[] trainInfo, String[] columns){
        model.setColumnCount(0);
        model.setRowCount(0);
        System.out.println(columns.length);

        for(String j : columns){
            model.addColumn(j);
        }

        model.addRow(trainInfo);
    }

    private TravelerView getTravelerView(){
        return this;
    }

    private void addB1Listener() {
        b1.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e){
                TravelerPresenter tp = new TravelerPresenter(getTravelerView());
                tp.searchTrains(getNumber(), getPointOfDeparture(), getDestination(), getDuration(), checkBox.isSelected());
            }
        });
    }

    private void addB2Listener() {
        b2.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e){
                TravelerPresenter tp = new TravelerPresenter(getTravelerView());
                tp.goToLogin();
                frame.dispose();
            }
        });
    }
}
